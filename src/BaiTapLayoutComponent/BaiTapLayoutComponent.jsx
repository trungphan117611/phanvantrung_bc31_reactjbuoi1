import React, { Component } from "react";
import Footer from "./footer";
import Header from "./header";
import Section from "./section";

export default class BaiTapLayoutComponent extends Component {
  render() {
    return (
      <div>
        <Header />
        <Section />
        <Footer />
      </div>
    );
  }
}
