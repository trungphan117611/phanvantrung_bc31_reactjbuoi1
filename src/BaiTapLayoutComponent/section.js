import React, { Component } from "react";

class Section extends Component {
  render() {
    return (
      <div>
        <section>
          <div className="container py-5 px-5">
            <div className="row gx-lg-5">
              <div className="col-lg-6 col-xxl-4 mb-5 ">
                <div className="card px-5 bg-light border-0 d-block">
                  <div class="feature bg-primary text-white mt-n4 rounded">
                    <i class="bi bi-cloud-download"></i>
                  </div>

                  <div className="title py-3">
                    <h2>Free to download</h2>
                    <p>
                      As always, Start Bootstrap has a powerful collectin of
                      free templates!
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-xxl-4 mb-5 ">
                <div className="card px-5 bg-light border-0 d-block">
                  <div class="feature bg-primary text-white mt-n4 rounded">
                    <i class="bi bi-collection"></i>
                  </div>

                  <div className="title py-3">
                    <h2>Fresh new layout</h2>
                    <p>
                      With Bootstrap 5, we've created a fresh new layout for
                      this template!
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-xxl-4 mb-5 ">
                <div className="card px-5 bg-light border-0 d-block">
                  <div class="feature bg-primary text-white mt-n4 rounded">
                    <i class="bi bi-card-heading"></i>
                  </div>

                  <div className="title py-3">
                    <h2>Jumbotron hero header</h2>
                    <p>
                      The heroic part of this template is the jumbotron hero
                      header!
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-xxl-4 mb-5 ">
                <div className="card px-5 bg-light border-0 d-block">
                  <div class="feature bg-primary text-white mt-n4 rounded">
                    <i class="bi bi-bootstrap"></i>
                  </div>

                  <div className="title py-3">
                    <h2>Feature boxes</h2>
                    <p>
                      We've created some custom feature boxes using Bootstrap
                      icons!
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-xxl-4 mb-5 ">
                <div className="card px-5 bg-light border-0 d-block">
                  <div class="feature bg-primary text-white mt-n4 rounded">
                    <i class="bi bi-code"></i>
                  </div>

                  <div className="title py-3">
                    <h2>Simple clean code</h2>
                    <p>
                      We keep our dependencies up to date and squash bugs as
                      they come!
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-xxl-4 mb-5 ">
                <div className="card px-5 bg-light border-0 d-block">
                  <div class="feature bg-primary text-white mt-n4 rounded">
                    <i class="bi bi-patch-check"></i>
                  </div>

                  <div className="title py-3">
                    <h2>A name you trust</h2>
                    <p>
                      Start Bootstrap has been the leader in free Bootstrap
                      templates since 2013!
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Section;
