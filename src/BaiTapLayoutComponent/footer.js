import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div>
        <div className="footer bg-dark py-4">
          <div className="container text-white">
            <p>Copyright © Your Website 2022</p>
          </div>
        </div>
      </div>
    );
  }
}
