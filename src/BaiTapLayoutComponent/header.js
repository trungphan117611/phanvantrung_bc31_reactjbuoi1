import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="container px-lg-5">
            <a className="navbar-brand" href="#">
              Start Bootstrap
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" />
            </button>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav mr-auto"></ul>
              <form className="form-inline my-2 my-lg-0">
                <ul className="navbar-nav mr-auto">
                  <li className="nav-item active">
                    <a className="nav-link nav-item active text-white" href="#">
                      Home <span className="sr-only">(current)</span>
                    </a>
                  </li>
                  <li className="nav-item active">
                    <a className="nav-link" href="#">
                      About <span className="sr-only">(current)</span>
                    </a>
                  </li>
                  <li className="nav-item active">
                    <a className="nav-link" href="#">
                      Contact <span className="sr-only">(current)</span>
                    </a>
                  </li>
                </ul>
              </form>
            </div>
          </div>
        </nav>
        <header className="">
          <div className="container py-5 px-5 ">
            <div className="py-5 px-5  bg-light">
              <h1 className="font-weight-bold">A warm welcome!</h1>
              <h5 className="px-5 pb-4">
                Bootstrap utility classes are used to create this jumbotron
                since the old component has been removed from the framework. Why
                create custom CSS when you can use utilities?
              </h5>
              <button type="button" class="btn btn-primary btn-lg ">
                Call to action
              </button>
            </div>
          </div>
        </header>
      </div>
    );
  }
}
